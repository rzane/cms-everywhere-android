package com.example.cmseverywhere;


import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter; 
import android.widget.ListView;
import android.widget.Toast;


public class PostList extends Activity implements OnItemClickListener {
	private ListView lvmenu;
	private ArrayAdapter<Post> adapter;
	private ArrayList<Post> posts;
	dbHelper databaseHelper;
	
	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        //databaseHelper = new dbHelper(this);
        
        posts = Post.getPosts();
                
        lvmenu = (ListView) findViewById(R.id.lvmenu);
               
        adapter = new ArrayAdapter<Post>(this, R.layout.listitem, posts);
              
        lvmenu.setAdapter(adapter);
        lvmenu.setOnItemClickListener(this);
        registerForContextMenu(lvmenu);
    }
	
    @Override
    public void onCreateContextMenu (ContextMenu menu, View view, ContextMenuInfo menuInfo)
    {
    	super.onCreateContextMenu(menu, view, menuInfo);
    	getMenuInflater().inflate(R.menu.postlistcontextmenu, menu);
    }
    
    
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
    	AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
    	int position = menuInfo.position;
    	int admin = User.getCurUseradmin();
    	Post heldpost = posts.get(position);

    	switch (item.getItemId())
    	{
    		case R.id.DeletePost:    	
		    	Post post = (Post) lvmenu.getItemAtPosition(position);
		    	//databaseHelper = new CrimeDatabaseHelper(this);
		    	//SQLiteDatabase database;
		    	//database = databaseHelper.getWritableDatabase();
		    	
				if (admin == 1) {
					post.deletePost(heldpost.getPost_id());
					adapter.remove(post);
					adapter.notifyDataSetChanged();
				} else if (admin == 0) {
					Toast.makeText(PostList.this, "Only an administrator can add or delete content", Toast.LENGTH_LONG).show();
				}
				break;
			
    		case R.id.EditPost:
    			Intent intentep = new Intent(PostList.this, EditPost.class);  			
    			intentep.putExtra("post_id", heldpost.getPost_id());
    			intentep.putExtra("postTitle", heldpost.getPostTitle());
    			intentep.putExtra("postContent", heldpost.getPostContent());
    			intentep.putExtra("postType", heldpost.getPostType());
    			startActivity(intentep);
    			break;
    	}
		
    	return true;
    }
	
	public void onItemClick
	(AdapterView<?> parent, View view, int position, long id) 
	{
		Post post = posts.get(position);
		Intent intentEditPost = new Intent(PostList.this , EditPost.class);
		//Integer postID = post.getPost_id();
		intentEditPost.putExtra("post_id", post.getPost_id());
		intentEditPost.putExtra("postTitle", post.getPostTitle());
		intentEditPost.putExtra("postContent", post.getPostContent());
		intentEditPost.putExtra("postType", post.getPostType());
		startActivity(intentEditPost);	
	}
	
	
}
