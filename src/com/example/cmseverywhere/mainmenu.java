package com.example.cmseverywhere;


import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter; 
import android.widget.ListView;
import android.widget.Toast;


public class mainmenu extends Activity implements OnItemClickListener {
    
	private ListView lvmenu;
	//dbHelper databaseHelper;
	User user;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        
        //databaseHelper = new dbHelper(this);
        
        // SQLiteDatabase database;
        //database = databaseHelper.getReadableDatabase();
        
		//Bundle dataBundle = getIntent().getExtras();
		//String userLogin = User.getCurUserLogin();
        //user = User.getUserByLogin(userLogin, database);
		Integer admin = User.getCurUseradmin();
		
		String[] userSections = new String[] {"Static Blocks", "Blog", "Images"};
		String[] adminSections = new String[] {"Static Blocks", "Blog", "Images", "Add New Post", "Add New User"};
		
        lvmenu = (ListView)findViewById(R.id.lvmenu);
        
		if (admin == 0) {
			lvmenu.setAdapter(new ArrayAdapter<String>(this, R.layout.listitem, userSections));
		} else if (admin == 1) {
			lvmenu.setAdapter(new ArrayAdapter<String>(this, R.layout.listitem, adminSections));
		}
        
        
        lvmenu.setOnItemClickListener(this);
    }
	public void onItemClick
	(AdapterView<?> parent, View view, int position, long id) 
	{
		if (position == 0) {
			Intent intentListPosts = new Intent(mainmenu.this , PostList.class);
			startActivity(intentListPosts);
		} else if (position == 1) {
			Toast.makeText(mainmenu.this, "Blog Section Still Under Development", Toast.LENGTH_LONG).show();
			//Intent intentListBlogs = new Intent(mainmenu.this, BlogList.class);
			//startActivity(intentListBlogs);
		} else if (position == 2) {
			Toast.makeText(mainmenu.this, "Images Section Still Under Development", Toast.LENGTH_LONG).show();
			//Intent intentListImages = new Intent(mainmenu.this, ImageList.class);
			//startActivity(intentListImages);
		} else if (position == 3) {
			Intent intentAddPost = new Intent(mainmenu.this, NewPost.class);
			startActivity(intentAddPost);
		} else if (position == 4) {
			Intent intentadduser = new Intent(mainmenu.this, AddNewUser.class);
			startActivity(intentadduser);
		}
		//Intent intentListPosts = new Intent(mainmenu.this , PostList.class);
		//startActivity(intentListPosts);	
	}
}

