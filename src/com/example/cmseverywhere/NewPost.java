package com.example.cmseverywhere;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NewPost extends Activity implements OnClickListener 
{
	private EditText etEditPostTitle;
	private EditText etEditPostContent;
	private Button btnUpdatePost;
	private dbHelper dbHelper;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editcontent);
		
		dbHelper = new dbHelper(this);        
		
		etEditPostTitle = (EditText) findViewById(R.id.editPostTitle);
		etEditPostContent = (EditText) findViewById(R.id.editPostContent);
		btnUpdatePost = (Button) findViewById(R.id.updatePost);
		
		etEditPostTitle.setTextColor(Color.BLACK);
		etEditPostContent.setTextColor(Color.BLACK);
		
		btnUpdatePost.setOnClickListener(this);
		}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		etEditPostTitle = (EditText) findViewById(R.id.editPostTitle);
		etEditPostContent = (EditText) findViewById(R.id.editPostContent);
		btnUpdatePost = (Button) findViewById(R.id.updatePost);
		
		String postTitle = etEditPostTitle.getText().toString();
		String postContent = etEditPostContent.getText().toString();
		Integer postType = 0;
		Integer postAuthorID = User.getCurUserID();
		Post post = new Post (postContent, postTitle, postType);
		post.insertPost(postType, postContent, postTitle, postAuthorID);
		
		Intent intentpl = new Intent(NewPost.this, PostList.class);
		startActivity(intentpl);
	}
}
