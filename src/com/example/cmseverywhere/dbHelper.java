package com.example.cmseverywhere;
import android.content.Context;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class dbHelper extends SQLiteOpenHelper {

	
	private static final String FILENAME = "cmseverywhere.sqlite";
	private static final int VERSION = 7;
	public dbHelper(Context context)
	{
		super(context, FILENAME, null, VERSION);
	}
	//If database does not exist, onCreate method is called
	@Override
	public void onCreate(SQLiteDatabase database) 
	{
		createTables(database);
	}

	//If database exists and new version (different from existing version number)
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) 
	{
			database.execSQL("drop table if exists posts");
			database.execSQL("drop table if exists users");
			database.execSQL("drop table if exists uplink");
			
			createTables(database);
	}
	public void createTables(SQLiteDatabase database)
	{
		database.execSQL("create table posts ( post_id integer primary key autoincrement," +
				" postContent varchar(60), postType bit(1) not null, postTitle varchar(40));" );
		database.execSQL("create table users ( user_id integer primary key autoincrement," +
				" userLogin varchar(60) not null, userPass varchar(60) not null, admin bit(1) not null);" );
		database.execSQL("create table uplink ( user_id integer not null, post_id integer not null, " +
				"foreign key (user_id) references users (user_id), " +
				"foreign key (post_id) references posts (post_id) );" );
		
		//PrePopulateTablesWithData(database);
	}//end of createTables
	/*
	public void PrePopulateTablesWithData (SQLiteDatabase database)
	{
		String userLogin = "rzane";
		String userPass = "test123";
		Integer admin = 1;
		User user = new User (userLogin, userPass, admin);
		user.insertUser(database);
		
		userLogin = "user";
		userPass = "test123";
		admin = 0;
		user = new User (userLogin, userPass, admin);
		user.insertUser(database);
		
		String postContent = "This is a sample post. Blah Blah Blah.";
		String postTitle = "Sample 1";
		Integer postType = 0;
		Post post = new Post (postContent, postTitle, postType);
		post.insertPost(database);
		
		postContent = "This is another sample post. Blah.";
		postTitle = "Sample 2";
		postType = 0;
		post = new Post (postContent, postTitle, postType);
		post.insertPost(database);		
		
	}
	*/
}
