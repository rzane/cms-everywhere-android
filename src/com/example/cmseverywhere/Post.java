package com.example.cmseverywhere;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import android.annotation.SuppressLint;
import android.os.AsyncTask;

public class Post {
	private Integer post_id;
	private String postContent;
	private String postTitle;
	private Integer postType;
	private static final String BASEURL = "http://cmseverywhere.webhop.org/api/posts/";
	
	public Post(String postContent, String postTitle, Integer postType)
	{
		this.postContent = postContent;
		this.postTitle = postTitle;
		this.postType = postType;
	}
	
	public Post(Integer post_id, String postContent, String postTitle, Integer postType)
	{
		this.post_id = post_id;
		this.postContent = postContent;
		this.postTitle = postTitle;
		this.postType = postType;
	}
	public Integer getPost_id() {
		return post_id;
	}
	public void setPost_id(Integer post_id) {
		this.post_id = post_id;
	}

	public String getPostContent() {
		return postContent;
	}
	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}
	public String getPostTitle() {
		return postTitle;
	}
	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}
	public Integer getPostType() {
		return postType;
	}
	public void setPostType(Integer postType) {
		this.postType = postType;
	}
	public void insertPost(Integer postType, String postContent, String postTitle, Integer postAuthorID) {
	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("postType", Integer.toString(postType));
	    data.put("postContent", postContent);
	    data.put("postTitle", postTitle);
	    data.put("postAuthorID", Integer.toString(User.getCurUserID()));
	    AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);
	    asyncHttpPost.execute(BASEURL + "insertPost.php");
	}
	
	public void updatePost(Integer postID, String postContent, String postTitle) {
	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("postID", Integer.toString(postID));
	    data.put("postContent", postContent);
	    data.put("postTitle", postTitle);
	    data.put("postAuthorID", Integer.toString(User.getCurUserID()));
	    AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);
	    asyncHttpPost.execute(BASEURL + "updatePost.php");
	}

	@SuppressLint("NewApi")
	public static ArrayList<Post> getPosts()
	{
		ArrayList<Post> fetchedPosts =	new ArrayList<Post>();
		/*
		//select * from crime_type order by crime_type_description asc;
		String table = "posts";
		String[] columns = null;
		String selection = "postType=0";
		String[] selectionArgs = null;
		String groupBy = null;
		String having = null;
		String orderBy = null;
		
		//Data access using cursors
		cursor = database.query
				(table, columns, selection, selectionArgs, groupBy, having, orderBy);
		
		Post post = null;
		
		cursor.moveToFirst();
		
		while(!cursor.isAfterLast())
		{
			post = getPostFromCursor(cursor, database);
			fetchedPosts.add(post);
			cursor.moveToNext();
		}
		
		cursor.close();
		*/
		Post post;
		
		String URL = BASEURL + "getposts.php?format=xml";
		String KEY_ITEM = "post";
		String KEY_ID = "postID"; // parent node
		String KEY_TITLE = "postTitle";
		String KEY_CONTENT = "postContent";
		String KEY_TYPE = "postType";
		
		XMLGetter getter = new XMLGetter();
		String[] params = new String[] {URL};
		AsyncTask<String, Void, String> XMLWebServiceTask = getter.execute(params);
		String xml;
		
		try {
			xml = XMLWebServiceTask.get();
			XMLParser parser = new XMLParser();
			//String xml = getter.doInBackground(URL); // getting XML
			Document doc = parser.getDomElement(xml); // getting DOM element
			 
			NodeList nl = doc.getElementsByTagName(KEY_ITEM);
			
			// looping through all item nodes <item>
			for (int i = 0; i < nl.getLength(); i++) {
	            Element e = (Element) nl.item(i);
			    Integer postID = Integer.parseInt(parser.getValue(e, KEY_ID));
			    String postContent = parser.getValue(e, KEY_CONTENT);
			    String postTitle = parser.getValue(e, KEY_TITLE);
			    Integer postType = Integer.parseInt(parser.getValue(e, KEY_TYPE));
			    post = new Post(postID, postContent, postTitle, postType);
			    fetchedPosts.add(post);
			}
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return fetchedPosts;
	}
	
	public static ArrayList<Post> getBlogs()
	{
		ArrayList<Post> fetchedBlogs =	new ArrayList<Post>();
		Post post;
		
		String URL = BASEURL + "getblogs.php?format=xml";
		String KEY_ITEM = "post";
		String KEY_ID = "postID"; // parent node
		String KEY_TITLE = "postTitle";
		String KEY_CONTENT = "postContent";
		String KEY_TYPE = "postType";
		
		XMLGetter getter = new XMLGetter();
		String[] params = new String[] {URL};
		AsyncTask<String, Void, String> XMLWebServiceTask = getter.execute(params);
		String xml;
		
		try {
			xml = XMLWebServiceTask.get();
			XMLParser parser = new XMLParser();
			//String xml = getter.doInBackground(URL); // getting XML
			Document doc = parser.getDomElement(xml); // getting DOM element
			 
			NodeList nl = doc.getElementsByTagName(KEY_ITEM);
			
			// looping through all item nodes <item>
			for (int i = 0; i < nl.getLength(); i++) {
	            Element e = (Element) nl.item(i);
			    Integer postID = Integer.parseInt(parser.getValue(e, KEY_ID));
			    String postContent = parser.getValue(e, KEY_CONTENT);
			    String postTitle = parser.getValue(e, KEY_TITLE);
			    Integer postType = Integer.parseInt(parser.getValue(e, KEY_TYPE));
			    post = new Post(postID, postContent, postTitle, postType);
			    fetchedBlogs.add(post);
			}
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return fetchedBlogs;
	}
	
	public static Post getPostUsingId(Integer post_id)
	{	
		
		Post fetchedpost = null;
		String postIDString = Integer.toString(post_id);
		
		String URL = BASEURL + "getpostbyID.php?format=xml&postID=" + postIDString;
		String KEY_ITEM = "post";
		String KEY_ID = "postID"; // parent node
		String KEY_TITLE = "postTitle";
		String KEY_CONTENT = "postContent";
		String KEY_TYPE = "postType";
		
		XMLGetter getter = new XMLGetter();
		String[] params = new String[] {URL};
		AsyncTask<String, Void, String> XMLWebServiceTask = getter.execute(params);
		String xml;
		try {
			xml = XMLWebServiceTask.get();
		
		XMLParser parser = new XMLParser();
		Document doc = parser.getDomElement(xml); // getting DOM element
		 
		NodeList nl = doc.getElementsByTagName(KEY_ITEM);
        Element e = (Element) nl.item(0);
	    Integer postID = Integer.parseInt(parser.getValue(e, KEY_ID));
	    String postContent = parser.getValue(e, KEY_CONTENT);
	    String postTitle = parser.getValue(e, KEY_TITLE);
	    Integer postType = Integer.parseInt(parser.getValue(e, KEY_TYPE));
	    fetchedpost = new Post(postID, postContent, postTitle, postType);	
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return fetchedpost;
		
	}
	/*
	public static Post getPostFromCursor(Cursor cursor, SQLiteDatabase database)
	{
		Integer post_id;
		String postContent;
		String postTitle;
		Integer postType;
		
		Post post;
		
		post_id = cursor.getInt(cursor.getColumnIndex("post_id"));
		postContent = cursor.getString(cursor.getColumnIndex("postContent"));
		postTitle = cursor.getString(cursor.getColumnIndex("postTitle"));
		postType = cursor.getInt(cursor.getColumnIndex("postType"));
		
		post = new Post(post_id, postContent, postTitle, postType);
		
		return post;
	}
	*/
	public String toString()
	{
		return  getPostTitle();
	}
	
	public void deletePost(Integer post_id) {
		
	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("postID", Integer.toString(post_id));
	    AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);
	    asyncHttpPost.execute(BASEURL + "deletePost.php");
	}


	
}
