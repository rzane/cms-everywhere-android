package com.example.cmseverywhere;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class AddNewUser extends Activity implements OnClickListener {
	
	
	private EditText etUserLogin;
	private EditText etUserPass;
	private CheckBox cbAdmin;
	private Button btnAddUser;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.adduser);
		etUserLogin = (EditText) findViewById(R.id.editUserPass);
		etUserPass = (EditText) findViewById(R.id.editUserPass);
		cbAdmin = (CheckBox) findViewById(R.id.cbAdmin);
		btnAddUser = (Button) findViewById(R.id.adduser);
		
		etUserLogin.setTextColor(Color.BLACK);
		etUserPass.setTextColor(Color.BLACK);
		
		btnAddUser.setOnClickListener(this);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		String userLogin = etUserLogin.getText().toString();
		String userPass = etUserPass.getText().toString();
		Integer admin;
		if (cbAdmin.isChecked()) {
			admin = 1;
		}
		else {
			admin = 0;
		}
		User user = new User(userLogin, userPass, admin);
		user.insertUser(userLogin, userPass, admin);
		Toast.makeText(this, "User Added", Toast.LENGTH_SHORT).show();
		
		Intent intentadduser = new Intent(AddNewUser.this, mainmenu.class);
		startActivity(intentadduser);
	}
}
