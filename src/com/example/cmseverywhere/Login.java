package com.example.cmseverywhere;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {
	
	private String username;
	private String password;
	//dbHelper databaseHelper;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        //Font fix for password field hint
        final EditText password = (EditText) findViewById(R.id.pwInput);
        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_login, menu);
        return true;
        
    }
    
    public void onLoginClick(View v) {
        //databaseHelper = new dbHelper(this);
        //SQLiteDatabase database;
        //database = databaseHelper.getReadableDatabase();
    	final EditText txtUsername = (EditText) findViewById(R.id.userInput);
    	final EditText txtPassword = (EditText) findViewById(R.id.pwInput);
    	username = txtUsername.getText().toString();
    	password = txtPassword.getText().toString();
    	boolean tf = User.Login(username, password);
    	String failedMessage = "Login Failed. Please try again.";
    	
    	if (tf)
    	{
    		Intent intentGoToMenu = new Intent(Login.this, mainmenu.class);
    		startActivity(intentGoToMenu);
    	}else{
    		Toast.makeText(Login.this, failedMessage, Toast.LENGTH_LONG).show();
    	}
    }
    
    
}
