package com.example.cmseverywhere;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditPost extends Activity implements OnClickListener 
{
	private EditText etEditPostTitle;
	private EditText etEditPostContent;
	private Button btnUpdatePost;
	//private SQLiteDatabase database;
	private Post selectedpost;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editcontent);
		setTitle("Edit Post");
		
		Bundle dataBundle = getIntent().getExtras();
		Integer post_id = dataBundle.getInt("post_id");
		String postTitle = dataBundle.getString("postTitle");
		String postContent = dataBundle.getString("postContent");
		//selectedpost = Post.getPostUsingId(post_id);
		
		etEditPostTitle = (EditText) findViewById(R.id.editPostTitle);
		etEditPostContent = (EditText) findViewById(R.id.editPostContent);
		btnUpdatePost = (Button) findViewById(R.id.updatePost);
		
		etEditPostTitle.setTextColor(Color.BLACK);
		etEditPostContent.setTextColor(Color.BLACK);
		
        //dbHelper = new dbHelper(this);        
        //database = dbHelper.getReadableDatabase();

		etEditPostTitle.setText(postTitle);
        etEditPostContent.setText(postContent);
        
		btnUpdatePost.setOnClickListener(this);
		//String newPostTitle = etEditPostTitle.getText().toString();
		//String newPostContent = etEditPostContent.getText().toString();
		//Integer postType = 0;
		
		//Post post = new Post (newPostContent, newPostTitle, postType);
		//post.updatePost(post_id, database);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Bundle dataBundle = getIntent().getExtras();
		Integer post_id = dataBundle.getInt("post_id");
		Integer postType = dataBundle.getInt("postType");
		
		etEditPostTitle = (EditText) findViewById(R.id.editPostTitle);
		etEditPostContent = (EditText) findViewById(R.id.editPostContent);
		btnUpdatePost = (Button) findViewById(R.id.updatePost);
		
		String newPostTitle = etEditPostTitle.getText().toString();
		String newPostContent = etEditPostContent.getText().toString();
		
		
		Post post = new Post (newPostContent, newPostTitle, postType);
		post.updatePost(post_id, newPostContent, newPostTitle);
		Intent intentPostList = new Intent(EditPost.this, PostList.class);
		startActivity(intentPostList);
	}
}
