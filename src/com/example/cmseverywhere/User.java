package com.example.cmseverywhere;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;

public class User {
	private Integer user_id;
	private String userLogin;
	private String userPass;
	private Integer admin;
	private static String curUserLogin;
	private static String curUserPass;
	private static Integer curUseradmin;
	private static Integer curUserID;
	private static final String BASEURL = "http://cmseverywhere.webhop.org/api/users/";
	
	public User(String userLogin, String userPass, Integer admin)
	{
		this.userLogin = userLogin;
		this.userPass = userPass;
		this.admin = admin;
	}
	
	public User(Integer user_id, String userLogin, String userPass, Integer admin)
	{
		this.user_id = user_id;
		this.userLogin = userLogin;
		this.userPass = userPass;
		this.admin = admin;
	}
	
	public void insertUser(String userLogin, String userPass, Integer admin)
	{
		//database = getWritableDatabase();
	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("userLogin", userLogin);
	    data.put("userPass", userPass);
	    data.put("admin", Integer.toString(admin));
	    AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);
	    asyncHttpPost.execute(BASEURL + "insertUser.php");
	}
	
	public static boolean Login(String username, String password)
    {  
	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("userLogin", username);
	    data.put("userPass", password);
	    AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);
	    asyncHttpPost.execute(BASEURL + "gettheuser.php");
	    
		//String KEY_ITEM = "Login";
		//String KEY_ID = "AuthStatus"; // parent node
		
		String xml;
		
		String KEY_ITEM = "user";
		String KEY_ID = "userID"; // parent node
		String KEY_LOGIN = "userLogin";
		String KEY_PASS = "userPass";
		String KEY_ADMIN = "admin";
		
		User user;
		try {
			xml = asyncHttpPost.get();
			XMLParser parser = new XMLParser();
			Document doc = parser.getDomElement(xml);
			
			NodeList nl = doc.getElementsByTagName(KEY_ITEM);
            Element e = (Element) nl.item(0);
            
		    Integer userID = Integer.parseInt(parser.getValue(e, KEY_ID));
		    String userLogin = parser.getValue(e, KEY_LOGIN);
		    String userPass = parser.getValue(e, KEY_PASS);
		    Integer admin = Integer.parseInt(parser.getValue(e, KEY_ADMIN));
		    
		    setCurUseradmin(admin);
			setCurUserPass(userPass);
			setCurUserLogin(userLogin);
			setCurUserID(userID);
		    
		    user = new User(userID, userLogin, userPass, admin);
			
			if (userID != null) {
				return true;
			}
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
		
		/*
        Cursor mCursor = database.rawQuery("SELECT * FROM USERS WHERE userLogin=? AND userPass=?", new String[]{username,password});  
        if (mCursor != null) {  
            if(mCursor.getCount() > 0)  
            {  
                return true;  
            }  
        }  
     return false;
     */  
    }
	/*
	public static ArrayList<User> getUsers(SQLiteDatabase database)
	{
		ArrayList<User> fetchedUsers =	new ArrayList<User>();
		
		//select * from crime_type order by crime_type_description asc;
		String table = "users";
		String[] columns = null;
		String selection = "null";
		String[] selectionArgs = null;
		String groupBy = null;
		String having = null;
		String orderBy = null;
		
		//Data access using cursors
		Cursor cursor = database.query
				(table, columns, selection, selectionArgs, groupBy, having, orderBy);
		
		User user = null;
		
		cursor.moveToFirst();
		
		while(!cursor.isAfterLast())
		{
			user = getUserFromCursor(cursor, database);
			fetchedUsers.add(user);
			cursor.moveToNext();
		}
		
		cursor.close();
		
		return fetchedUsers;
	}
	
	public static User getUserFromCursor(Cursor cursor, SQLiteDatabase database)
	{
		Integer user_id;
		String userLogin;
		String userPass;
		Integer admin;
		
		User user;
		
		user_id = cursor.getInt(cursor.getColumnIndex("user_id"));
		userLogin = cursor.getString(cursor.getColumnIndex("userLogin"));
		userPass = cursor.getString(cursor.getColumnIndex("userPass"));
		admin = cursor.getInt(cursor.getColumnIndex("admin"));
		
		user = new User(user_id, userLogin, userPass, admin);
		
		return user;
	}
	*/
	
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public Integer getAdmin() {
		return admin;
	}
	public void setAdmin(Integer admin) {
		this.admin = admin;
	}
	/*
	public static User getUserByLogin(String userLogin, SQLiteDatabase database) {
			User user = null;
			
			String table = "users";
			String[] columns = null;
			String selection = "userLogin = ?";
			String[] selectionArgs = new String[] {userLogin};
			String groupBy = null;
			String having = null;
			String orderBy = null;
			
			//Data access using cursors
			Cursor cursor = database.query
					(table, columns, selection, selectionArgs, groupBy, having, orderBy);		
			
			cursor.moveToFirst();
					
			if(!cursor.isAfterLast())
			{			
				user = getUserFromCursor(cursor, database);	
			}

			cursor.close();
			
			setCurUseradmin(user.getAdmin());
			setCurUserPass(user.getUserPass());
			setCurUserLogin(user.getUserLogin());
			setCurUserID(user.getUser_id());
			
			return user;
	}
	*/
	

	public static String getCurUserLogin() {
		return curUserLogin;
	}

	public static void setCurUserLogin(String curUserLogin) {
		User.curUserLogin = curUserLogin;
	}

	public static String getCurUserPass() {
		return curUserPass;
	}

	public static void setCurUserPass(String curUserPass) {
		User.curUserPass = curUserPass;
	}

	public static Integer getCurUseradmin() {
		return curUseradmin;
	}

	public static void setCurUseradmin(Integer curUseradmin) {
		User.curUseradmin = curUseradmin;
	}

	public static Integer getCurUserID() {
		return curUserID;
	}

	public static void setCurUserID(Integer curUserID) {
		User.curUserID = curUserID;
	}
	
}
